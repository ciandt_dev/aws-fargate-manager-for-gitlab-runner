package main

import (
	"os"

	"gitlab.com/ciandt_dev/aws-fargate-manager-for-gitlab-runner/manager"
)

func main() {
	manager.Run(manager.NewStep(os.Args[2], os.Args[1]))
}
