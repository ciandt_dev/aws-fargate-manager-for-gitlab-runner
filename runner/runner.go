package runner

//Job represent the job to be run in a specific environment.
type Job struct {
	Name    string
	Content string
	Params  map[string]string
}

//JobResult inform the specific details about the job execution
type JobResult struct {
	Status int
	Desc   string
	JobID  string
	Error  error
}

//JobRunner is capable of star the execution of a job
// It will be responsible for start and manage the execution.
// this is a blocking function
type JobRunner interface {
	Run(Job) JobResult
}

//New creates a new Job definiton
func New(name string, content string, params map[string]string) Job {
	return Job{Name: name, Content: content, Params: params}
}
