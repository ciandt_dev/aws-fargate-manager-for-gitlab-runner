package runner

type fargateRunner struct{}

//NewFargateRunner create a new JobRunner capable of running an AWS Fargate Job
func NewFargateRunner() JobRunner {
	return fargateRunner{}
}

func (f fargateRunner) Run(job Job) JobResult {
	result := JobResult{Status: 0}

	// try to upload job files to the S3 bucket
	s3bucket, error := uploadJobContent(job)
	if error != nil {
		result.Status = -1
		result.Desc = "Failed to upload content to S3 bucket"
		result.Error = error
		return result
	}

	// try to register the new taskDefinition
	taskDefinition, error := regiterTaskDefinition(job, s3bucket)
	if error != nil {
		result.Status = -2
		result.Desc = "Failed to register/update task definition"
		result.Error = error
		return result
	}

	// create the default network configuration
	networkConfiguration := createNetworkConfiguration(job)

	// try to run the task
	task, error := runTask(taskDefinition, networkConfiguration)
	if error != nil {
		result.Status = -2
		result.Desc = "Failed to register/update task definition"
		result.Error = error
		return result
	}
	result.JobID = task

	// wait for the task to end read the logs

	// Determine job ended with success or failure

	return result
}

func uploadJobContent(job Job) (string, error) {
	return "", nil
}

func regiterTaskDefinition(job Job, s3bucket string) (string, error) {
	return "", nil
}

func createNetworkConfiguration(job Job) string {
	return ""
}

func runTask(taskDefinition string, networkConfiguration string) (string, error) {
	return "", nil
}
