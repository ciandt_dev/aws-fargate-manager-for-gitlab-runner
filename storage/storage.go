package storage

//Storage is used to store step and step content for latter execution
type Storage interface {
	//Save the current step
	//Param 1 is the script file
	//param 2 is the step name
	SaveStepContent(src, step string) error

	//Dump the content to single file (zip) and clean the storage
	//Param 1 is the desired file name
	DumpToFile(string) error
}
