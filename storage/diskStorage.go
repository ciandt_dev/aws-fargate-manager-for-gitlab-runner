package storage

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/ciandt_dev/aws-fargate-manager-for-gitlab-runner/compress"
)

const dirMode = 0755
const dirName = "scripts"
const ordeFile = "script_order.txt"

type diskStorage struct {
	name  string
	index string
}

//New storage to save the steps and step contents for latter execution
func New() Storage {
	storage := diskStorage{name: dirName, index: dirName + string(os.PathSeparator) + ordeFile}

	_, err := os.Stat(storage.name)
	if os.IsNotExist(err) {
		error := os.Mkdir(storage.name, dirMode)
		handle(error)
	} else {
		handle(err)
	}

	_, err = os.Stat(storage.index)
	if os.IsNotExist(err) {

	}

	return storage
}

func (s diskStorage) SaveStepContent(src, step string) error {
	dst := s.generageStepKey(step)

	s.saveSequence(dst)
	Log("Saving file in storage", src, "-->", dst)

	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destination.Close()
	_, err = io.Copy(destination, source)
	return err
}

func (s diskStorage) DumpToFile(fn string) error {
	//compress the current scripts
	error := compress.ZipFiles(fn, s.listStepsContent())

	if nil != error {
		return error
	}

	return os.RemoveAll(s.name)
}

func (s diskStorage) listStepsContent() []string {
	files := []string{}

	err := filepath.Walk(s.name, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})

	handle(err)

	return files
}

func (s diskStorage) saveSequence(item string) {
	Log("Saving sequence for:", item, "on", s.index)
	fileHandle, _ := os.OpenFile(s.index, os.O_APPEND, 0666)
	writer := bufio.NewWriter(fileHandle)
	defer fileHandle.Close()

	fmt.Fprintln(writer, item)
	writer.Flush()
}

func (s diskStorage) generageStepKey(step string) string {
	return s.name + string(os.PathSeparator) + step + ".sh"
}
