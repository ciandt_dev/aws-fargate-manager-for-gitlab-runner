package storage

import (
	"fmt"
	"os"
)

func handle(error error) {
	if nil != error {
		//TODO: Use gitlab Log package
		Log("Unexpected error:", error.Error())
		abordProcess()
	}
}

func abordProcess(m ...string) {
	Log("Aborting execution")
	os.Exit(1)
}

//Log print the messages in the stdout
func Log(m ...string) {
	//TODO: Use gitlab log package
	fmt.Println(m)
}
