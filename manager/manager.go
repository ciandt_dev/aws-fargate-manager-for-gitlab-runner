package manager

import (
	"gitlab.com/ciandt_dev/aws-fargate-manager-for-gitlab-runner/runner"
	"gitlab.com/ciandt_dev/aws-fargate-manager-for-gitlab-runner/storage"
)

const taskLaunchStep = "archive_cache"
const file = "build.zip"

// Step is represents one gitlab job step
type Step struct {
	Name       string
	ScriptFile string
}

// NewStep creates a new step for the manager to execute
func NewStep(name string, script string) Step {
	return Step{Name: name, ScriptFile: script}
}

//Run execute the step necessari elements
func Run(step Step) {
	Log("Executing AWS Fargate Manager for step:", step.Name)

	storage := preRun(step.ScriptFile, step.Name)

	run(step.Name, storage)

}

func run(stepName string, s storage.Storage) {
	Log("Run: phase started")

	if taskLaunchStep == stepName {
		error := s.DumpToFile(file)
		handle(error)

		//TODO: pass on the gitlab job id
		job := runner.New("build-job", file, buildEnv())
		fargateRunner := runner.NewFargateRunner()

		fargateRunner.Run(job)

	} else {
		Log("Nothing to do now.")
	}

	Log("Run: phase DONE")
}

func buildEnv() map[string]string {
	return map[string]string{}
}

func preRun(scriptFile string, stepName string) storage.Storage {
	Log("PreRun: phase started")

	//save the script files to send to executor
	storage := storage.New()
	error := storage.SaveStepContent(scriptFile, stepName)
	handle(error)

	Log("PreRun: phase DONE")
	return storage
}
